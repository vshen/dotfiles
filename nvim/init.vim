source ~/.vimrc
nnoremap <silent> <BS> :TmuxNavigateLeft<CR>
let g:deoplete#enable_at_startup = 1
